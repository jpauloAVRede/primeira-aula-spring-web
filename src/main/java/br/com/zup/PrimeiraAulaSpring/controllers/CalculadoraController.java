package br.com.zup.PrimeiraAulaSpring.controllers;

import br.com.zup.PrimeiraAulaSpring.dtos.ImcDTO;
import br.com.zup.PrimeiraAulaSpring.dtos.MediaPonderadaDTO;
import br.com.zup.PrimeiraAulaSpring.dtos.SomaDTO;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculadoraController {

    @PostMapping("/soma")
    public int soma(@RequestBody SomaDTO somaDTO){ //@RequestBody - tá passando algo no corpo da requisitção;
        return somaDTO.getPrimeiroNumero() + somaDTO.getSegundoNumero();
    }

    @PostMapping("/mediaPonderada")
    public double mediaPonderada(@RequestBody MediaPonderadaDTO mediaPonderadaDTO) {
        double mediaPomperada;

        mediaPomperada = (((mediaPonderadaDTO.getPrimeiroNumero()*1) + (mediaPonderadaDTO.getSegundoNumero()*2) + (mediaPonderadaDTO.getTerceiroNumero()*3) + (mediaPonderadaDTO.getQuartoNumero()*4))/(1+2+3+4));

        return mediaPomperada;
    }

    @PostMapping("/calculadoraIMC")
    public double calculadoraIMC(@RequestBody ImcDTO imcDTO) {
        double imc = 0;

        imc = (imcDTO.getPeso()/(imcDTO.getAltura()*imcDTO.getAltura()));

        return imc;
    }

}
