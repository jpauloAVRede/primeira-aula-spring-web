package br.com.zup.PrimeiraAulaSpring.controllers;

import br.com.zup.PrimeiraAulaSpring.dtos.ListaProdutoDTO;
import br.com.zup.PrimeiraAulaSpring.dtos.ProdutoInDTO;
import br.com.zup.PrimeiraAulaSpring.dtos.ProdutoOutDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ListaProdutoController {

    private List<ProdutoOutDTO> listaProduto = new ArrayList<>();

    @GetMapping("/listaProdutos")
    public List<ProdutoOutDTO> listaProdutos() {
        return listaProduto;
    }

    @PostMapping("/addProduto")
    public void adicionarListaProduto(@RequestBody ProdutoInDTO produtoInDTO) {
        ProdutoOutDTO  produtoOutDTO = new ProdutoOutDTO(produtoInDTO.getNome(), produtoInDTO.getDescricao(), produtoInDTO.getQuantidade(), produtoInDTO.getPreco());

        listaProduto.add(produtoOutDTO);

    }

}
