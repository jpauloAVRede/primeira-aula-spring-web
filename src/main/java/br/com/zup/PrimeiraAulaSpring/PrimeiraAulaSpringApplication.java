package br.com.zup.PrimeiraAulaSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimeiraAulaSpringApplication {

	public static void main(String[] args) {
		//abaixo o SpringApplication assume o controle dentro da maquina virtual, pois ele sabe o que tá vai fazer;
		//não mexer nessa classe main;
		SpringApplication.run(PrimeiraAulaSpringApplication.class, args);
	}

}
