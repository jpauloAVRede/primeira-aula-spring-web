package br.com.zup.PrimeiraAulaSpring.dtos;

public class ImcDTO {
    private double altura;
    private double peso;

    public ImcDTO() {}

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }
}
