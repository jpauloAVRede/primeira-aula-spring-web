package br.com.zup.PrimeiraAulaSpring.dtos;

public class MediaPonderadaDTO {

        private double primeiroNumero;
        private double segundoNumero;
        private double terceiroNumero;
        private double quartoNumero;

        //padrão java bin para que seja representada no jason

        public MediaPonderadaDTO(){}

        public double getPrimeiroNumero() {
            return primeiroNumero;
        }

        public void setPrimeiroNumero(int primeiroNumero) {
            this.primeiroNumero = primeiroNumero;
        }

        public double getSegundoNumero() {
            return segundoNumero;
        }

        public void setSegundoNumero(int segundoNumero) {
            this.segundoNumero = segundoNumero;
        }

        public double getTerceiroNumero() {
            return terceiroNumero;
        }

        public void setTerceiroNumero(int terceiroNumero) {
            this.terceiroNumero = terceiroNumero;
        }

        public double getQuartoNumero() {
            return quartoNumero;
        }

        public void setQuartoNumero(int quartoNumero) {
            this.quartoNumero = quartoNumero;
        }
}
