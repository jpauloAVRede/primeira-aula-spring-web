package br.com.zup.PrimeiraAulaSpring.dtos;

//toda classe DTO é uma representação do jason
//pode ser entrar do cliente ou saída para o cliente;
//nenhuma DTO tem relacionamento com banco de dados;
//DTO: objeto de transferencia de dados
public class SomaDTO {
    private int primeiroNumero;
    private int segundoNumero;

    //padrão java bin para que seja representada no jason

    public SomaDTO(){}

    public int getPrimeiroNumero() {
        return primeiroNumero;
    }

    public void setPrimeiroNumero(int primeiroNumero) {
        this.primeiroNumero = primeiroNumero;
    }

    public int getSegundoNumero() {
        return segundoNumero;
    }

    public void setSegundoNumero(int segundoNumero) {
        this.segundoNumero = segundoNumero;
    }

}
